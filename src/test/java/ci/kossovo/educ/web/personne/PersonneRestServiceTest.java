/*package ci.kossovo.educ.web.personne;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import ci.kossovo.educ.entites.Invite;
import ci.kossovo.educ.entites.Personne;
import ci.kossovo.educ.metier.IPersonneMetier;
import ci.kossovo.educ.web.controller.PersonneRestService;

@RunWith(SpringRunner.class)
@WebMvcTest(PersonneRestService.class)
public class PersonneRestServiceTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private IPersonneMetier personneMetierMock;

	private ObjectMapper mapper = new ObjectMapper();

	@Test
	public void trouverTousLesInvites() throws Exception {

		Personne p1 = new Invite("Mr", "Diarra", "Drissa", "C01");
		p1.setId(1L);
		p1.setType("IN");
		Personne p2 = new Invite("Mr", "Traore", "Abdoulaye", "C02");
		p2.setId(2L);
		p2.setType("IN");

		// obtenir une liste non modifiable de personne
		
		List<Personne> pers = Arrays.asList(p1, p2);

		given(personneMetierMock.personneALL("IN")).willReturn(pers);

		// alors
		this.mvc.perform(get("/typepersonnes/IN")).andExpect(status().isOk())
				.andExpect(jsonPath("$.body.length()", is(2))).andExpect(jsonPath("$.body.[0].nom", is("Diarra")));

	}

	@Test
	public void creerUnePersonne() throws Exception {

		Personne p1 = new Invite("Mr", "Diarra", "Drissa", "C01");

		Personne p2 = p1;
		p2.setId(3L);
		// quand on donne
		given(personneMetierMock.creer(any(Personne.class))).willReturn(p2);

		// alors
		this.mvc.perform(post("/personnes").contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8).content(mapper.writeValueAsString(p1)))
				.andExpect(status().isOk()).andExpect(jsonPath("$.statut", is(0)))
				.andExpect(jsonPath("$.body.id", is(3))).andExpect(jsonPath("$.body.nom", is("Diarra")));

	}

	@Test
	public void modifierUnePersonne() throws Exception {

		Personne pExistant = new Invite("Mr", "Diarra", "Drissa", "C01");
		pExistant.setId(1L);
		Personne pModifier = pExistant;
		pModifier.setNom("Traore");

		// quand on donne
		given(personneMetierMock.findById(pModifier.getId())).willReturn(pExistant);
		given(personneMetierMock.modifier(any(Personne.class))).willReturn(pModifier);

		// alors
		this.mvc.perform(put("/personnes").contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8).content(mapper.writeValueAsString(pModifier)))
				.andExpect(status().isOk()).andExpect(jsonPath("$.statut", is(0)))
				.andExpect(jsonPath("$.body.id", is(1))).andExpect(jsonPath("$.body.nom", is("Traore")));

	}

	@Test
	public void trouverUnePresonne() throws Exception {

		Personne p1 = new Invite("Mr", "Diarra", "Drissa", "C01");
		p1.setId(1L);
		p1.setType("IN");

		given(personneMetierMock.findById(1L)).willReturn(p1);

		// alors
		this.mvc.perform(get("/personnes/{id}",p1.getId())).andExpect(status().isOk())
		.andExpect(jsonPath("$.body.id", is(1)))
				.andExpect(jsonPath("$.statut", is(0)))
		.andExpect(jsonPath("$.body.nom", is("Diarra")));

	}

	@Test
	public void supprimerUnePresonne() throws Exception {

		Personne p1 = new Invite("Mr", "Diarra", "Drissa", "C01");
		p1.setId(1L);
		p1.setType("IN");

		given(personneMetierMock.findById(1L)).willReturn(p1);
		given(personneMetierMock.supprimer(p1.getId())).willReturn(true);

		// alors
		this.mvc.perform(delete("/personnes/{id}",p1.getId())).andExpect(status().isOk())
		
				.andExpect(jsonPath("$.statut", is(0)))
		.andExpect(jsonPath("$.body", is(true)));

	}
	
}
*/