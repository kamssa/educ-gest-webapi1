/*package ci.kossovo.educ.web.personne;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import ci.kossovo.educ.entites.Matiere;
import ci.kossovo.educ.metier.ImatiereMetier;
import ci.kossovo.educ.web.controller.MatiereRestService;

@RunWith(SpringRunner.class)
@WebMvcTest(MatiereRestService.class)
public class MatiereRestServiceTest {
	@Autowired
	private MockMvc mvc;

	@MockBean
	private ImatiereMetier matiereMetierMock;

	private ObjectMapper mapper = new ObjectMapper();
	@Test
	public void creerUneMatiere() throws Exception {

		Matiere m = new Matiere("UML", "Architecture");

		Matiere m1 = m;
		m1.setId(3L);
		// quand on donne
		given(matiereMetierMock.creer(any(Matiere.class))).willReturn(m1);

		// alors
		this.mvc.perform(post("/postMatiers").contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8).content(mapper.writeValueAsString(m1)))
				.andExpect(status().isOk()).andExpect(jsonPath("$.statut", is(0)))
				.andExpect(jsonPath("$.body.id", is(3))).andExpect(jsonPath("$.body.libelle", is("UML")));

	}

}
*/