package ci.kossovo.educ.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import ci.kossovo.educ.EducGestMetierDaoJpa1Application;

@SpringBootApplication
@Import(EducGestMetierDaoJpa1Application.class)
public class EducGestWebApi1Application {

	public static void main(String[] args) {
		SpringApplication.run(EducGestWebApi1Application.class, args);
	}
}
