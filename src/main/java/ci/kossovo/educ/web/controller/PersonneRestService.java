package ci.kossovo.educ.web.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ci.kossovo.educ.entites.Personne;
import ci.kossovo.educ.exceptions.InvalideEducException;
import ci.kossovo.educ.metier.IPersonneMetier;
import ci.kossovo.educ.web.models.Reponse;
import ci.kossovo.educ.web.utilitaire.Static;

@RestController
@CrossOrigin
public class PersonneRestService {
	@Autowired
	private IPersonneMetier personneMietier;
  // l'objet jsonMapper permet de serialiser/deserialiser des objets en json et le convertir en objet
	@Autowired
	private ObjectMapper jsonMapper;

	private Reponse<Personne> getPersonneById(Long id) {
		Personne personne = null;
		try {
			personne = personneMietier.findById(id);
		} catch (RuntimeException e) {
			new Reponse<Personne>(1, Static.getErreursForException(e), null);
		}
		if (personne == null) {
			List<String> messages = new ArrayList<>();
			messages.add(String.format("la personne n'exixte pas", id));
			return new Reponse<Personne>(2, messages, null);
		}
		return new Reponse<Personne>(0, null, personne);
	}

	// ajout d'une personne
	@PostMapping("/personnes")
	public String creer(@RequestBody Personne p) throws JsonProcessingException {
		Reponse<Personne> reponse;

		try {
			Personne p1 = personneMietier.creer(p);
			List<String> messages = new ArrayList<>();
			messages.add(String.format("%s %s a ete enregistrer avec succes",p1.getNom(),
					p1.getPrenom()));
			reponse = new Reponse<Personne>(0, messages, p1);

		} catch (InvalideEducException e) {

			reponse = new Reponse<Personne>(1, Static.getErreursForException(e), null);
		}
		return jsonMapper.writeValueAsString(reponse);
	}

	// modification
	@PutMapping("/personnes")
	public String modifier(@RequestBody Personne modif) throws JsonProcessingException {
		Reponse<Personne> reponsePersModif = null;
		Reponse<Personne> reponse = null;
		boolean erreur = false;
		// on recupere la personnae a modifier
		if (!erreur) {
			reponsePersModif = getPersonneById(modif.getId());
			if (reponsePersModif.getStatut() != 0) {
				reponse = new Reponse<Personne>(reponsePersModif.getStatut(), reponsePersModif.getMessages(), null);
				erreur = true;
			}
		}
		if (!erreur) {
			// Personne entity = null;

			try {

				Personne p2 = personneMietier.modifier(modif);
				List<String> messages = new ArrayList<>();
				messages.add(String.format("[%s %s] a modifier avec succes", p2.getNom(),
						p2.getPrenom()));
				reponse = new Reponse<Personne>(0, messages, p2);

			} catch (InvalideEducException e) {

				reponse = new Reponse<Personne>(1, Static.getErreursForException(e), null);
			}

		}
		return jsonMapper.writeValueAsString(reponse);

	}

	// personne par type
	@GetMapping("/typePersonnes/{type}")
	public String findAllTypePersonne(@PathVariable("type") String type) throws JsonProcessingException {
		Reponse<List<Personne>> reponse;

		try {
			List<Personne> personneTous = personneMietier.personneALL(type);
			if (!personneTous.isEmpty()) {
				reponse = new Reponse<List<Personne>>(0, null, personneTous);
			} else {
				List<String> messages = new ArrayList<>();
				messages.add("Pas de personnes enregistrées");
				reponse = new Reponse<List<Personne>>(1, messages, new ArrayList<>());
			}

		} catch (Exception e) {

			reponse = new Reponse<>(1, Static.getErreursForException(e), null);
		}
		return jsonMapper.writeValueAsString(reponse);
	}

	// personne par id
	@GetMapping("/personnes/{id}")
	public String chercherParId(@PathVariable("id") Long id) throws JsonProcessingException {

		Reponse<Personne> reponse = null;
		reponse = getPersonneById(id);

		return jsonMapper.writeValueAsString(reponse);

	}

	@DeleteMapping("/personnes/{id}")
	public String supprimer(@PathVariable("id") Long id) throws JsonProcessingException {

		Reponse<Boolean> reponse = null;
		boolean erreur = false;
		Personne p=null;
		if (!erreur) {
			Reponse<Personne> responseSup = getPersonneById(id);
			p=responseSup.getBody();
			if (responseSup.getStatut() != 0) {
				reponse = new Reponse<>(responseSup.getStatut(), responseSup.getMessages(), null);
				erreur = true;

			}
		}
		if (!erreur) {
			// suppression
			try {
				
				List<String> messages = new ArrayList<>();
				messages.add(String.format("[%s][%s  %s] a ete supprime",p.getId(),p.getNom(),p.getPrenom() ));
						

				reponse = new Reponse<Boolean>(0, messages, personneMietier.supprimer(id));

			} catch (RuntimeException e1) {
				reponse = new Reponse<>(3, Static.getErreursForException(e1), null);
			}
		}
		return jsonMapper.writeValueAsString(reponse);
	}

}
