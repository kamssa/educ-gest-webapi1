package ci.kossovo.educ.web.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ci.kossovo.educ.entites.Matiere;
import ci.kossovo.educ.exceptions.InvalideEducException;
import ci.kossovo.educ.metier.ImatiereMetier;
import ci.kossovo.educ.web.models.Reponse;
import ci.kossovo.educ.web.utilitaire.Static;

@RestController
@CrossOrigin
public class MatiereRestService {

	// injedction de l'interface metier dans mon service
	@Autowired
	private ImatiereMetier matierMetier;
	// injection de ObjetMapper
	@Autowired
	private ObjectMapper jsonMapper;
	
	
	private Reponse<Matiere> getMatiereById(Long id){
		Matiere matiere = null;
		try {
			matiere= matierMetier.findById(id);
		} catch (RuntimeException e) {
			new Reponse<>(1, Static.getErreursForException(e),null );
		}
		if(matiere==null) {
			List< String> messages = new ArrayList<>();
			messages.add(String.format("la matiere n'existe pas", id));
			new Reponse<>(2, messages, null);
			
		}
		return new Reponse<Matiere>(0, null, matiere);
	}

	// ajout d'une matiere dans la base de donnee
	@PostMapping("/matieres")
	public String creerMatiere(@RequestBody Matiere mat) throws JsonProcessingException {
		Reponse<Matiere> reponse;
		
		
			
			try {
				Matiere mat1 = matierMetier.creer(mat);

				// creation d'une liste de String appele message pour affiche le message de succes
				List<String> messages = new ArrayList<>();
				messages.add(String.format("%s a ete ajoute avec succes ", mat1.getLibelle()));

				reponse = new Reponse<Matiere>(0, messages, mat1);
				
					
					
			} catch (InvalideEducException e) {
				reponse = new Reponse<Matiere>(1, Static.getErreursForException(e), null);
			}
		
		
		return jsonMapper.writeValueAsString(reponse);
	}

	

	// ajout d'une matiere dans la base de donnee
	@PutMapping("/matieres")
	public String modfierUneMatiere(@RequestBody Matiere mat) throws JsonProcessingException {

		Reponse<Matiere> reponse = null;
		// on recupere le libelle de l'objet a modifier
		List<Matiere> mats = matierMetier.findByLibelle(mat.getLibelle());
		// on verifie si la collection est null alors la matiere n'existe pas
		if (mats.isEmpty()) {

			// modfier l'element
			Matiere mat1=null;
			try {
				mat1 = matierMetier.creer(mat);
			} catch (InvalideEducException e) {
				
				e.printStackTrace();
			}
			// creation d'une liste de String appele message
			List<String> messages = new ArrayList<>();
			messages.add(String.format("%s a ete modifie avec succes ", mat1.getLibelle()));
			// on recupere le message et le corps dans un objet reponse qui sera retourne
			reponse = new Reponse<Matiere>(0, messages, mat1);
		} else {
			List<String> messages1 = new ArrayList<>();
			messages1.add(String.format("%s n'a pu etre modifie ", mat.getLibelle()));
		}

		return jsonMapper.writeValueAsString(reponse);
	}

	// recuperer toutes les matieres de la base de donnee
	@GetMapping("/matieres")
	public String findAllMatiere() throws JsonProcessingException, InvalideEducException {
		Reponse<List<Matiere>> reponse;
		try {
			List<Matiere> mats =matierMetier.findAll();
			reponse = new Reponse<List<Matiere>>(0, null, mats);
		} catch (Exception e) {
			reponse = new Reponse<>(1, Static.getErreursForException(e), null);
		}
		return jsonMapper.writeValueAsString(reponse);

	}
	//Annotation @PathVariable qui indique qu'un paramètre de méthode doit être lié à une variable de modèle URI
	@GetMapping("/matieres/{id}")
	public String chercherMatiereParId (@PathVariable Long id) throws JsonProcessingException{
		Reponse<Matiere> reponse = null;
		reponse= getMatiereById(id);
		return jsonMapper.writeValueAsString(reponse);
		
		
	}
	@GetMapping("/recherche/{mc}")
	public String chercherParMc(@PathVariable String mc) throws JsonProcessingException {
		Reponse<List<Matiere>> reponse = null;
		List<Matiere> matieres;
		try {
			matieres = matierMetier.chercherMatiereParMc(mc);
			reponse = new Reponse<List<Matiere>>(0, null, matieres);
		} catch (Exception e) {
			new Reponse<>(1, Static.getErreursForException(e), null);
					}
		
		return jsonMapper.writeValueAsString(reponse);
		
	}
}
